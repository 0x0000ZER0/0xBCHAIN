#ifndef Z0_BCHAIN_H
#define Z0_BCHAIN_H

#include <stdint.h>
#include <time.h>
#include <stdbool.h>

#include "z0_sha256.h"

typedef struct {
	uint32_t ref_no;
	uint8_t  from_acc[8];
	uint8_t  to_acc[8];
	double   quantity;
} z0_deal;

typedef struct {
	uint8_t   prev_hash[32];
	uint8_t   curr_hash[32];
	time_t    curr_time;
	uint32_t  attempts;

	uint32_t  d_len;
	z0_deal  *deals;	
} z0_block;

typedef struct {
	uint32_t  quality;
	double    reward;
	double	  delta;

	uint32_t  p_cap;
	uint32_t  p_len;
	z0_deal  *p_deals;

	uint32_t  b_cap;
	uint32_t  b_len;
	z0_block *blocks;
} z0_chain;

void
z0_deal_init(z0_deal*, uint8_t*, uint8_t*, double);

void
z0_block_genesis(z0_block*);

void
z0_block_mine(z0_block*, uint32_t);

void
z0_block_print(z0_block*);

void
z0_chain_init(z0_chain*, uint32_t, double, double);

void
z0_chain_deal_push(z0_chain*, z0_deal*);

void
z0_chain_deals_print(z0_chain*);

void
z0_chain_mine(z0_chain*, uint8_t*);

double
z0_chain_balance(z0_chain*, uint8_t*);

bool
z0_chain_validate(z0_chain*);

void
z0_chain_blocks_print(z0_chain*);

void
z0_chain_free(z0_chain*);

#endif
