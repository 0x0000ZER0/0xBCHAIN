
main:
	gcc -O2 -o 0xBCHAIN -Wall -pedantic -I0xSHA256  0xSHA256/z0_sha256.c main.c z0_block.c z0_chain.c z0_deal.c

clear:
	rm ./0xBCHAIN
