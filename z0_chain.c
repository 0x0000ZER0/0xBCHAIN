#include "z0_bchain.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void
z0_chain_init(z0_chain *chain, uint32_t quality, double reward, double delta) {
	chain->quality = quality;
	chain->reward  = reward;
	chain->delta   = delta;

	chain->p_cap   = 4;
	chain->p_len   = 0;
	chain->p_deals = malloc(chain->p_cap * sizeof (z0_deal));

	chain->b_cap   = 4;
	chain->b_len   = 1;
	chain->blocks  = malloc(chain->b_cap * sizeof (z0_block));
	
	z0_block_genesis(&chain->blocks[0]);
}

void
z0_chain_deal_push(z0_chain *chain, z0_deal *deal) {
	if (chain->p_len == chain->p_cap) {
		chain->p_cap   *= 2;
		chain->p_deals  = realloc(chain->p_deals, chain->p_cap * sizeof (z0_deal));
	}

	z0_deal *p_deal;
	p_deal = &chain->p_deals[chain->p_len];

	p_deal->ref_no = deal->ref_no;
	memcpy(p_deal->from_acc, deal->from_acc, 8 * sizeof (uint8_t));
	memcpy(p_deal->to_acc, deal->to_acc, 8 * sizeof (uint8_t));
	p_deal->quantity = deal->quantity;

	++chain->p_len;
}

void
z0_chain_deals_print(z0_chain *chain) {
	if (chain->p_deals == NULL) {
		printf("Z0 INFO: NULL");	
		return;
	}
	
	for (uint32_t i = 0; i < chain->p_len; ++i) {
		printf("Z0 INFO: %u\n", chain->p_deals[i].ref_no);
		printf("Z0 INFO: %s\n", chain->p_deals[i].from_acc);
		printf("Z0 INFO: %s\n", chain->p_deals[i].to_acc);
		printf("Z0 INFO: %.2f\n", chain->p_deals[i].quantity);
	}
}

void
z0_chain_mine(z0_chain *chain, uint8_t *acc) {
	if (chain->reward <= 0.0)
		return;

	static uint8_t admin[] = "ACADMIN";

	z0_deal deal;
	z0_deal_init(&deal, admin, acc, chain->reward);
	z0_chain_deal_push(chain, &deal);

	if (chain->b_len == chain->b_cap) {
		chain->b_cap  *= 2;
		chain->blocks  = realloc(chain->blocks, chain->b_cap * sizeof (z0_block));
	}
	
	z0_block *prev;
	prev = &chain->blocks[chain->b_len - 1];

	z0_block *block;
	block = &chain->blocks[chain->b_len];

	memcpy(block->prev_hash, prev->curr_hash, 32 * sizeof (uint8_t));
	time(&block->curr_time);

	block->d_len = chain->p_len;
	block->deals = malloc(block->d_len * sizeof (z0_deal));
	memcpy(block->deals, chain->p_deals, chain->p_len * sizeof (z0_deal));

	block->attempts = 0;

	z0_block_mine(block, chain->quality);

	++chain->b_len;

	chain->p_len   = 0;
	chain->reward -= chain->delta;	
}

double
z0_chain_balance(z0_chain *chain, uint8_t acc[8]) {
	double balance;
	balance = 0.0;

	int res;
	for (uint32_t i = 0; i < chain->b_len; ++i) {
		z0_block *block;
		block = &chain->blocks[i];

		for (uint32_t j = 0; j < block->d_len; ++j) {
			z0_deal *deal;
			deal = &block->deals[j];

			res = strncmp((char*)acc, (char*)deal->from_acc, 8 * sizeof (uint8_t));
			if (res == 0)
				balance -= deal->quantity;

			res = strncmp((char*)acc, (char*)deal->to_acc, 8 * sizeof (uint8_t));
			if (res == 0)
				balance += deal->quantity;	
		}	
	}		

	return balance;	
}

bool
z0_chain_validate(z0_chain *chain) {
	uint8_t temp_hash[32];

	z0_block *curr;
	z0_block *prev;

	int res;

	int len;
	len = chain->b_len - 1;
	while (len > 0) {
		curr = &chain->blocks[len];

		uint8_t *temp;
		temp = malloc(32 * sizeof (uint8_t)    + 
			      sizeof (curr->curr_time) +
			      sizeof (curr->attempts)  +
			      sizeof (curr->d_len)     +
			      curr->d_len * sizeof (z0_deal));
		
		uint64_t off;
		
		memcpy(temp, curr->prev_hash, 32 * sizeof (uint8_t));
		off = 32 * sizeof (uint8_t);

		memcpy(temp + off, &curr->curr_time, sizeof (curr->curr_time));
		off += sizeof (curr->curr_time);

		memcpy(temp + off, &curr->attempts, sizeof (curr->attempts));
		off += sizeof (curr->attempts);

		memcpy(temp + off, &curr->d_len, sizeof (curr->d_len)); 
		off += sizeof (curr->d_len);

		memcpy(temp + off, curr->deals, curr->d_len * sizeof (z0_deal));
		off += curr->d_len + sizeof (z0_deal);

		free(temp); 	
		
		res = strncmp((char*)temp_hash, (char*)curr->curr_hash, 32 * sizeof (uint8_t));
		if (res != 0)
			return false;

		prev = &chain->blocks[len - 1];
		res = strncmp((char*)prev->curr_hash, (char*)curr->prev_hash, 32 * sizeof (uint8_t)); 

		if (res != 0)
			return false;

		--len;
	}

	return true;
}

void
z0_chain_blocks_print(z0_chain *chain) {
	for (uint32_t i = 0; i < chain->b_len; ++i)
		z0_block_print(&chain->blocks[i]);
}

void
z0_chain_free(z0_chain *chain) {
	free(chain->p_deals);

	for (uint32_t i = 0; i < chain->b_len; ++i)
		if (chain->blocks[i].deals != NULL) 
			free(chain->blocks[i].deals);

	free(chain->blocks);
}

