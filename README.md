# 0xBCHAIN

A blockchain implementation in `C`.

### API

- Creating the blockchain.

```c
z0_chain chain;
z0_chain_init(&chain, 4, 10.5, 1.1);

//...

z0_chain_free(&chain);
```

- Adding transactions to it.

```c
uint8_t acc_001[] = "AC00001";
uint8_t acc_002[] = "AC00002";

z0_deal deal;

z0_deal_init(&deal, acc_001, acc_002, 3.2);
z0_chain_deal_push(&chain, &deal);

z0_deal_init(&deal, acc_002, acc_001, 1.7);
z0_chain_deal_push(&chain, &deal);
```

- Mining a block.

```c
uint8_t acc_miner[] = "ACMINER";
z0_chain_mine(&chain, acc_miner);
z0_chain_mine(&chain, acc_miner);
z0_chain_mine(&chain, acc_miner);
z0_chain_mine(&chain, acc_miner);

z0_chain_blocks_print(&chain);
```

- Getting the current balance.

```c
double balance;
balance = z0_chain_balance(&chain, acc_001);

printf("BALANCE: %.2f\n", balance);
```

- Validate the chain.

```c
bool is_valid;
is_valid = z0_chain_validate(&chain);

printf("IS VALID: %d\n", is_valid);
```
