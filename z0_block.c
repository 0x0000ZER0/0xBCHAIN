#include "z0_bchain.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void
z0_block_genesis(z0_block *block) {
	memset(block->prev_hash, 0, sizeof (block->prev_hash));
	time(&block->curr_time);
	
	block->attempts = 0;
	block->d_len 	= 0;
	block->deals	= NULL;	

	uint8_t *temp;
	temp = malloc(32 * sizeof (uint8_t) 	+ 
		      sizeof (block->curr_time) +
		      sizeof (block->attempts)  +
		      sizeof (block->d_len));

	uint64_t off;
	
	memcpy(temp, block->prev_hash, 32 * sizeof (uint8_t));	
	off = 32 * sizeof (uint8_t);

	memcpy(temp + off, &block->curr_time, sizeof (block->curr_time));
	off += sizeof (block->curr_time);

	memcpy(temp + off, &block->attempts, sizeof (block->attempts));
	off += sizeof (block->attempts);

	memcpy(temp + off, &block->d_len, sizeof (block->d_len));
	off += sizeof (block->d_len);

	z0_sha256_hash(temp, off, block->curr_hash);	

	free(temp);
}

void
z0_block_mine(z0_block *block, uint32_t quality) {
	uint8_t *temp;
	temp = malloc(32 * sizeof (uint8_t)     +
		      sizeof (block->curr_time) +
		      sizeof (block->attempts)  +
		      sizeof (block->d_len)     +
                      block->d_len * sizeof (z0_deal));

	uint64_t off;
	uint64_t off_attm;
	
	memcpy(temp, block->prev_hash, 32 * sizeof (uint8_t));
	off = 32 * sizeof (uint8_t);

	memcpy(temp + off, &block->curr_time, sizeof (block->curr_time));
	off += sizeof (block->curr_time);
	off_attm = off;

	memcpy(temp + off, &block->attempts, sizeof (block->attempts));
	off += sizeof (block->attempts);

	memcpy(temp + off, &block->d_len, sizeof (block->d_len));
	off += sizeof (block->d_len);

	memcpy(temp + off, block->deals, block->d_len * sizeof (z0_deal));
	off += block->d_len * sizeof (z0_deal);

	z0_sha256_hash(temp, off, block->curr_hash);

	uint8_t *pattern;
	pattern = calloc(quality, sizeof (uint8_t));

	int res;
	while (true) {
		res = strncmp((char*)block->curr_hash, (char*)pattern, quality * sizeof (uint8_t));
		if (res == 0)
			break;
		
		++block->attempts;
		memcpy(temp + off_attm, &block->attempts, sizeof (block->attempts));
			
		z0_sha256_hash(temp, off, block->curr_hash);
	}

	free(pattern);
	free(temp);
}

void
z0_block_print(z0_block *block) {
	printf("========================== Z0 BLOCK ==========================\n");

	z0_sha256_print(block->prev_hash);
	z0_sha256_print(block->curr_hash);
	printf("Z0 TIME: %s", ctime(&block->curr_time));
	printf("Z0 ATTM: %u\n", block->attempts);
	printf("Z0 DLEN: %u\n", block->d_len);
	
	if (block->deals != NULL) {
		for (uint32_t i = 0; i < block->d_len; ++i) {
			printf("-------------- Z0 DEAL --------------\n");
			printf("\tZ0 REF:  %u\n", block->deals[i].ref_no);
			printf("\tZ0 FROM: %s\n", block->deals[i].from_acc);
			printf("\tZ0 TO:   %s\n", block->deals[i].to_acc);
			printf("\tZ0 QUAN: %.2f\n", block->deals[i].quantity);
		}			
	} else {
		printf("Z0 INFO: NULL\n");
	}		
}


