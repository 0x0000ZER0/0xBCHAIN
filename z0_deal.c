#include "z0_bchain.h"

#include <string.h>

void
z0_deal_init(z0_deal *deal, uint8_t *from, uint8_t *to, double quantity) {
	static uint32_t ref = 0;
	
	if (quantity <= 0)
		return;

	deal->ref_no = ref;
	memcpy(deal->from_acc, from, 8 * sizeof (uint8_t));
	memcpy(deal->to_acc, to, 8 * sizeof (uint8_t));
	deal->quantity = quantity;

	++ref;	
}
